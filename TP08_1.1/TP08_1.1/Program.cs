﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP08_1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string user_value = "20";
                int value = int.Parse(user_value);
                Console.WriteLine("This line will never be executed");
            }
            catch (Exception)
            {
                Console.WriteLine("Error during user value conversion.");
            }
            Console.Read();
        }
    }
}
