﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP08_1._2
{
    class Program
    {
        public static double RacineCarree(double valeur)
        {
            if (valeur <= 0)
                throw new ArgumentOutOfRangeException("valeur", "Le paramètre doit être positif");
            return Math.Sqrt(valeur);
        }

        static void Main(string[] args)
        {
            Console.WriteLine(RacineCarree(-48));
            Console.Read();
        }
    }
}
