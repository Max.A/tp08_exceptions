﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP08_1._3
{
    class Program
    {
        public class MultipleDivision
        {
            private List<int> values;
            public MultipleDivision(List<int> values)
            {
                this.values = values;
            }

            public double Division(int index_dividende, int index_diviseur)
            {
                double quotient;
                quotient = this.values[index_dividende] / this.values[index_diviseur];
                return quotient;
            }
        }

        static void Main(string[] args)
        {
            string dividende_s, diviseur_s;
            int dividende, diviseur;
            int compteur = 0;
            List<int> values = new List<int> { 17, 12, 15, 38, 29, 157, 89, -22, 0, 5 };
            MultipleDivision mltd = new MultipleDivision(values);
            
            foreach (int valeur in values)
            {
                Console.WriteLine("[{0}] : {1}",compteur,valeur);
                compteur = compteur + 1;
            }

            Console.WriteLine("Saisir l'index du dividende :");
            dividende_s = Console.ReadLine();
            Int32.TryParse(dividende_s, out dividende);

            Console.WriteLine("Saisir l'index du diviseur :");
            diviseur_s = Console.ReadLine();
            Int32.TryParse(diviseur_s, out diviseur);
            
            Console.WriteLine(mltd.Division(dividende, diviseur));

            Console.Read();
        }
    }
}
